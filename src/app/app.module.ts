import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule }    from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { ListaAnimaisPageModule } from './animais/lista/lista.module';
import { DetalhesPageModule } from './animais/detalhes/detalhes.module';

import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { LoginPage } from './pages/auth/login/login.page';
import { RegisterPage } from './pages/auth/register/register.page';
import { FormsModule } from '@angular/forms';
import { LoginPageModule } from './pages/auth/login/login.module';
import { RegisterPageModule } from './pages/auth/register/register.module';
import { CalendarModule } from "ion2-calendar";


@NgModule({
  declarations: [AppComponent],
  entryComponents: [LoginPage, RegisterPage],
  imports: [
    FormsModule,
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    ListaAnimaisPageModule,
    DetalhesPageModule,
    LoginPageModule,
    RegisterPageModule,
    CalendarModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    NativeStorage,
    ImagePicker,
    Crop,
    FileTransfer,
    NativeStorage
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
