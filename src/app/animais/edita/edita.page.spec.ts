import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditaPage } from './edita.page';

describe('EditaPage', () => {
  let component: EditaPage;
  let fixture: ComponentFixture<EditaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
