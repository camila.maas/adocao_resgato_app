import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { AnimaisService } from 'src/app/services/animais/animais.service';
import { DomSanitizer } from '@angular/platform-browser';
import { AlertService } from 'src/app/services/alert.service';
import { NavController } from '@ionic/angular';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-edita',
  templateUrl: './edita.page.html',
  styleUrls: ['./edita.page.scss'],
})
export class EditaPage implements OnInit {
  public animal;
  private desabilitaCidade = true;
  public estados: Array<any>;
  public cidades: Array<any>;

  constructor(
    public route: ActivatedRoute,
    private animaisService: AnimaisService,
    private sanitizer: DomSanitizer,
    private alertService: AlertService,
    private navCtrl: NavController,
  ) { 
    this.route.paramMap.subscribe((params: ParamMap) => {
      console.log("id recebido: " + params.get("id"));
      this.animaisService.getDetalhesAnimal(params.get("id")).subscribe(data => {
        let retorno = (data as any)._body;
        data["get_fotos"].forEach(a=>a.caminho = this.sanitizer.bypassSecurityTrustUrl(a.caminho));
        this.animal = data;
        console.log(this.animal);

      }, error => { 
        console.log(error);
      })
    })
  }

  ngOnInit() {
    this.animaisService.getEstados().subscribe((data: any) => {
      let retorno = (data as any)._body;
      this.estados = data;

    }, error => {
      console.log(error);
    })
  }

  busca_cidades(id_estado) {
    this.animaisService.getCidades(id_estado).subscribe((data: any) => {
      let retorno = (data as any)._body;
      this.cidades = data;
      this.desabilitaCidade = false;
    }, error => {
      console.log(error);
    })
  }

  editar(formValue: NgForm) {
    this.mostraMensagem();
    this.animaisService.salvaAnimal(formValue.value)
      .subscribe(data => {
      }, error => {
        console.log(error);
      });
  }

  async mostraMensagem() {
    this.alertService.presentToast("Usuário editado!");
    this.navCtrl.navigateRoot('/meus-animais');
  }

}
