import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastraPage } from './cadastra.page';

describe('CadastraPage', () => {
  let component: CadastraPage;
  let fixture: ComponentFixture<CadastraPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastraPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastraPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
