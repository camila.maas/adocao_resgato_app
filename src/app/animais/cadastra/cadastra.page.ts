import { Component, OnInit } from '@angular/core';
import { AnimaisService } from 'src/app/services/animais/animais.service';
import { NgForm } from '@angular/forms';
// import { HttpClient, Headers, RequestOptions } from '@angular/common/http';
import { Crop } from '@ionic-native/crop/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-cadastra',
  templateUrl: './cadastra.page.html',
  styleUrls: ['./cadastra.page.scss'],
})
export class CadastraPage implements OnInit {

  public racas: Array<any>;
  private desabilitaRaca = true;
  private desabilitaCidade = true;
  public estados: Array<any>;
  public cidades: Array<any>;
  user: User;

  constructor(
    private animaisService: AnimaisService,
    private imagePicker: ImagePicker,
    private crop: Crop,
    private transfer: FileTransfer,
    private toastController: ToastController,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.animaisService.getEstados().subscribe((data: any) => {
      let retorno = (data as any)._body;
      this.estados = data;

    }, error => {
      console.log(error);
    })
  }

  busca_racas(tipo) {
    this.animaisService.getRacasTipo(tipo).subscribe((data: any) => {
      let retorno = (data as any)._body;
      this.racas = data;
      this.desabilitaRaca = false;
    }, error => {
      console.log(error);
    })
  }

  busca_cidades(id_estado) {
    this.animaisService.getCidades(id_estado).subscribe((data: any) => {
      let retorno = (data as any)._body;
      this.cidades = data;
      this.desabilitaCidade = false;
    }, error => {
      console.log(error);
    })
  }

  salvar(formValue: NgForm) {
    this.mostraMensagem();
    this.animaisService.salvaAnimal(formValue.value)
      .subscribe(data => {
      }, error => {
        console.log(error);
      });
  }

  cropUpload() {
    console.info('entrou');
    let options = {
      maximumImagesCount: 8,
      width: 500,
      height: 500,
      quality: 75
    }
  

    this.imagePicker.getPictures(options).then((results) => {
      for (let i = 0; i < results.length; i++) {
        console.log('Image URI: ' + results[i]);
        this.crop.crop(results[i], { quality: 100 })
          .then(
            newImage => {
              console.log('new image path is: ' + newImage);
              // const fileTransfer: FileTransferObject = this.transfer.create();
              const uploadOpts: FileUploadOptions = {
                fileKey: 'file',
                fileName: newImage.substr(newImage.lastIndexOf('/') + 1)
              };

              // fileTransfer.upload(newImage, 'http://192.168.0.7:3000/api/upload', uploadOpts)
              //   .then((data) => {
              //     console.log(data);
              //     this.respData = JSON.parse(data.response);
              //     console.log(this.respData);
              //     this.fileUrl = this.respData.fileUrl;
              //   }, (err) => {
              //     console.log(err);
              //   });
            },
            error => console.error('Error cropping image', error)
          );
      }
    }, (err) => { console.log(err); });
  }

  //Dados salvos
  async mostraMensagem() {

    const toast = await this.toastController.create({
      message: 'Anúncio aguardando aprovação!',
      position: 'bottom',
      buttons: [
        {
          side: 'end',
          text: 'Ok',
          handler: () => {
            this.router
            console.log('Favorite clicked');
            this.router.navigateByUrl('/tabs/lista');
          }
        }
      ]
    });

    toast.present();
  }

  ionViewDidEnter() {
    console.info('user');
    console.info(this.user);
    // this.authService.user().subscribe(
    //   user => {
    //     this.user = user;
    //   }
    // );
  }

}
