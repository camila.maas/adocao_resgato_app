import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { AnimaisService } from 'src/app/services/animais/animais.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.page.html',
  styleUrls: ['./detalhes.page.scss'],
})
export class DetalhesPage implements OnInit {
  public animal;

  constructor(
    public route: ActivatedRoute,
    private animaisService: AnimaisService,
    private sanitizer: DomSanitizer
  ) {
    this.route.paramMap.subscribe((params: ParamMap) => {
      console.log("id recebido: " + params.get("id"));
      this.animaisService.getDetalhesAnimal(params.get("id")).subscribe(data => {
        let retorno = (data as any)._body;
        data["get_fotos"].forEach(a => a.caminho = this.sanitizer.bypassSecurityTrustUrl(a.caminho));
        this.animal = data;
        console.log(this.animal);

      }, error => {
        console.log(error);
      })
    })
  }

  ngOnInit() {
  }

}
