import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaAnimaisPage } from './lista.page';

describe('ListaAnimaisPage', () => {
  let component: ListaAnimaisPage;
  let fixture: ComponentFixture<ListaAnimaisPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaAnimaisPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaAnimaisPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
