import { Component, OnInit, ViewChild } from '@angular/core';
import { LoadingController, IonInfiniteScroll, MenuController } from '@ionic/angular';
import { AnimaisService } from '../../services/animais/animais.service';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { IonTabs } from '@ionic/angular'
import { Router, ParamMap, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-lista',
  templateUrl: './lista.page.html',
  styleUrls: ['./lista.page.scss'],
})
export class ListaAnimaisPage implements OnInit {
  @ViewChild('tabs', null) tabs: IonTabs;

  // @ViewChild(IonInfiniteScroll,{static: true}) infiniteScroll: IonInfiniteScroll;

  public animais: Array<any>;
  public loading;
  public refresher;
  public isRefreshing: boolean = false;

  user: User;

  constructor(
    private http: HttpClientModule,
    private animaisService: AnimaisService,
    public loadingController: LoadingController,
    private sanitizer: DomSanitizer,
    public route: ActivatedRoute,
    private menu: MenuController, 
    private authService: AuthService
  ) {
    this.menu.enable(true);


  }

  ngOnInit() {
    // console.info('init animais');
    // console.info(this);
    this.carregarAnimais();
    // console.info('this.tabs.getSelected()');
    // console.info(this.tabs.getSelected());
  }

  loadData(event) {
    // console.info('animais');
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      // console.info('data');
      // console.info(event);
      // if (data.length == 1000) {
      //   event.target.disabled = true;
      // }
    }, 500);
  }

  carregarAnimais() {
    console.info('carregarAnimais animais');
    // console.info(window.localStorage.getItem('animais'));
    // console.info(this.route.url);
    this.presentLoading();

    this.animaisService.getAnimais().subscribe(
      (data: any) => {
        this.animais = data;

        this.animais.forEach(a => a.foto = this.sanitizer.bypassSecurityTrustUrl(a.foto));
        console.info('this.animais lista');
        console.info(this.animais);
        // console.info(this.isRefreshing);

        if (this.isRefreshing) {
          this.refresher.target.complete();
          this.isRefreshing = false;
        }
        // this.closeModal();
      }, error => {
        // console.log(error);
        if (this.isRefreshing) {
          this.refresher.target.complete();
          this.isRefreshing = false;
        }
        // this.closeModal();
      });
  }

  async presentLoading() {
    // console.info('animais');
    this.loading = await this.loadingController.create({
      message: 'Carregando animais',
      duration: 2000
    });
    // console.log('show');
    await this.loading.present();
    const { role, data } = await this.loading.onDidDismiss();

    // console.log('Loading dismissed!');
  }

  atualizar(refresher) {
    // console.info('refresher animais');
    // console.info(refresher);
    this.refresher = refresher;
    this.isRefreshing = true;
    this.carregarAnimais();
  }

  public getSantizeUrl(url: string) {
    // console.info('entrou animais');
    // console.info(url);
    return this.sanitizer.bypassSecurityTrustUrl(url);
    //return this.sanitizer.sanitize(2, 'url(' + url + ')');
  }

  ionViewWillLeave() {
    console.info('ionViewWillLeave');
  }

  ionViewDidLeave() {
    console.info('ionViewDidLeave');
  }

  ionViewWillUnload() {
    console.info('ionViewWillUnload');
  }

  ionViewCanEnter() {
    console.info('ionViewCanEnter');
  }

  ionViewCanLeave() {
    console.info('ionViewCanLeave');
  }

  teste() {
    console.info('teste entrou');
  }

  ionViewDidEnter() {
    console.info('user');
    console.info(this.user);
    // this.authService.user().subscribe(
    //   user => {
    //     this.user = user;
    //   }
    // );

    // this.sessaoService.usuario.subscribe((usuario:User)=>{
    //   this.user = usuario;
    // })
  }

}
