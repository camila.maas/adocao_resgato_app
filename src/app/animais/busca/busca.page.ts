import { Component, OnInit } from '@angular/core';
import { AnimaisService } from 'src/app/services/animais/animais.service';
import { NgForm } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { LoadingController } from '@ionic/angular';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-busca',
  templateUrl: './busca.page.html',
  styleUrls: ['./busca.page.scss'],
})
export class BuscaPage implements OnInit {
  public animais = [];
  public racas: Array<any>;
  private desabilitaRaca = true;
  private desabilitaCidade = true;
  public estados: Array<any>;
  public cidades: Array<any>;
  public loading;
  user: User;

  constructor(private animaisService: AnimaisService,
    private sanitizer: DomSanitizer,
    public loadingController: LoadingController,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.animaisService.getEstados().subscribe((data: any) => {
      let retorno = (data as any)._body;
      this.estados = data;

    }, error => {
      console.log(error);
    })
  }

  busca_racas(tipo) {
    this.animaisService.getRacasTipo(tipo).subscribe((data: any) => {
      let retorno = (data as any)._body;
      this.racas = data;
      this.desabilitaRaca = false;
    }, error => {
      console.log(error);
    })
  }

  busca_cidades(id_estado) {
    this.animaisService.getCidades(id_estado).subscribe((data: any) => {
      let retorno = (data as any)._body;
      this.cidades = data;
      this.desabilitaCidade = false;
    }, error => {
      console.log(error);
    })
  }

  buscar(formValue: NgForm) {
    this.presentLoading();
    this.animaisService.buscarFiltro(
      formValue.value).subscribe((data: any) => {
        this.animais.forEach(a => a.foto = this.sanitizer.bypassSecurityTrustUrl(a.foto));

        this.animais = data;
        this.loadingController.dismiss();
      }, error => {
        this.loading.onDidDismiss();
      });
  }

  async presentLoading() {
    // console.info('animais');
    this.loading = await this.loadingController.create({
      message: 'Buscando animais',
      // duration: 2000
    });
    // console.log('show');
    await this.loading.present();
    // const { role, data } = await this.loading.onDidDismiss();

    // console.log('Loading dismissed!');
  }
  ionViewDidEnter() {
    console.info('user');
    console.info(this.user);
    // this.authService.user().subscribe(
    //   user => {
    //     this.user = user;
    //   }
    // );
  }
}
