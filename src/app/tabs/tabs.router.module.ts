import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'busca',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../animais/lista/lista.module').then(m => m.ListaAnimaisPageModule)
          }
        ]
      },
      {
        path: '1',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../animais/lista/lista.module').then(m => m.ListaAnimaisPageModule)
          }
        ]
      },
      {
        path: '2',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../animais/lista/lista.module').then(m => m.ListaAnimaisPageModule)
          }
        ]
      },
    {
        path: 'landing',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/landing/landing.module').then(m => m.LandingPageModule)
          }
        ]
      },
    ]
  },
  {
    path: '',
    redirectTo: '/busca',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
