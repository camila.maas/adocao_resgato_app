import { Component, ViewChild } from '@angular/core';
import { AnimaisService } from '../services/animais/animais.service';
import { IonTabs, NavController } from '@ionic/angular'
import { ListaAnimaisPage } from '../animais/lista/lista.page';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  @ViewChild('tabs', null) tabs: IonTabs;
  // @ViewChild('lista', null) ListaAnimaisPage: ListaAnimaisPage;

  public animais = [];

  constructor(private animaisService: AnimaisService,
    public navCtrl: NavController,
    private router: Router
  ) { }

  ngOnInit() {
    // console.info('init');
    // console.info(this);
    //this.animaisService.carregarAnimais();
  }

  getSelectedTab() {
    console.info('changeTab');
    // this.animais = [];
    console.info(this.tabs.getSelected());

    this.animaisService.carregarAnimaisTipo(
      this.tabs.getSelected());

    // this.animais = JSON.parse(window.localStorage.getItem('animais'));
    // console.info('carregarAnimais animais');
    // console.info(window.localStorage.getItem('animais'));

    // console.info('this.animais');
    // console.info(this.animais);
  }

  teste() {
    console.info('teste callback');
  }

}
