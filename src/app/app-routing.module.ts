import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  // {
  //   path: '',
  //   redirectTo: 'busca',
  //   pathMatch: 'full'
  // },
  { path: '', loadChildren: './animais/busca/busca.module#BuscaPageModule' },
  // {
  //   path: '',
  //   redirectTo: 'lista',
  //   pathMatch: 'full'
  // },
  { path: 'landing', loadChildren: './pages/landing/landing.module#LandingPageModule' },  
  { path: 'login', loadChildren: './pages/auth/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/auth/register/register.module#RegisterPageModule' },
  
  { path: 'dashboard', loadChildren: './pages/dashboard/dashboard.module#DashboardPageModule', canActivate: [AuthGuard] },
  // { path: 'home', loadChildren: './home/home.module#HomePageModule', canActivate: [AuthGuard] },
  // { path: 'list', loadChildren: './list/list.module#ListPageModule', canActivate: [AuthGuard] },
  // { path: 'lista', loadChildren: './animais/lista/lista.module#ListaAnimaisPageModule' },
  { path: 'cadastra', loadChildren: './animais/cadastra/cadastra.module#CadastraPageModule' },
  { path: 'detalhes/:id', loadChildren: './animais/detalhes/detalhes.module#DetalhesPageModule' },
  { path: 'sobre', loadChildren: './sobre/sobre.module#SobrePageModule' },
  { path: 'lista-ong', loadChildren: './ong/lista/lista.module#ListaOngPageModule' },
  { path: 'detalhes-ong/:id', loadChildren: './ong/detalhes/detalhes.module#DetalhesPageModule' },
  { path: 'detalhes-evento/:id', loadChildren: './eventos/detalhes/detalhes.module#DetalhesPageModule' },
  { path: 'lista-eventos', loadChildren: './eventos/lista/lista.module#ListaPageModule' },
  { path: 'busca', loadChildren: './animais/busca/busca.module#BuscaPageModule' },
  { path: 'meus-animais', loadChildren: './perfil/meus-animais/meus-animais.module#MeusAnimaisPageModule' },
  { path: 'dados', loadChildren: './perfil/dados/dados.module#DadosPageModule' },
  { path: 'edita/:id', loadChildren: './animais/edita/edita.module#EditaPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}