import { Component, OnInit } from '@angular/core';
import { AnimaisService } from 'src/app/services/animais/animais.service';
import { AuthService } from 'src/app/services/auth.service';
import { UsuarioService } from 'src/app/services/usuarios/usuario.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-meus-animais',
  templateUrl: './meus-animais.page.html',
  styleUrls: ['./meus-animais.page.scss'],
})
export class MeusAnimaisPage implements OnInit {
  public usuario: any;
  public animais: Array<any>;

  constructor(
    private animaisService: AnimaisService,
    private authService: AuthService,
    private usuarioService: UsuarioService,
    private sanitizer: DomSanitizer

  ) { }

  ngOnInit() {
    // this.animaisService.getAnimaisUsuario().subscribe((data: any) => {
    //   let retorno = (data as any)._body;

    // }, error => {
    //   console.log(error);
    // })
    this.animaisService.getAnimaisUsuario(1).subscribe(data => {
      console.info('data');
      console.info(data);
      let retorno = (data as any)._body;
      data["foto"] = this.sanitizer.bypassSecurityTrustUrl(data["foto"]);
      // this.animais = data;

    }, error => {
      console.log(error);
    })
  }

  ionViewDidEnter() {
    console.info('user');
    console.info(this.usuario);
    // this.authService.user().subscribe(
    //   user => {
    //     this.usuario = user;
    //   }
    // );
  }
}
