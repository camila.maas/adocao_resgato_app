import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeusAnimaisPage } from './meus-animais.page';

describe('MeusAnimaisPage', () => {
  let component: MeusAnimaisPage;
  let fixture: ComponentFixture<MeusAnimaisPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeusAnimaisPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeusAnimaisPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
