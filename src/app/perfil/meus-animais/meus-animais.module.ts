import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MeusAnimaisPage } from './meus-animais.page';

const routes: Routes = [
  {
    path: '',
    component: MeusAnimaisPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MeusAnimaisPage]
})
export class MeusAnimaisPageModule {}
