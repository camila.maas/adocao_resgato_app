import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { NgForm } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { UsuarioService } from 'src/app/services/usuarios/usuario.service';
import { DomSanitizer } from '@angular/platform-browser';
import { AnimaisService } from 'src/app/services/animais/animais.service';
import { SessaoService } from 'src/app/services/sessao.service';
import { AlertService } from 'src/app/services/alert.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-dados',
  templateUrl: './dados.page.html',
  styleUrls: ['./dados.page.scss'],
})
export class DadosPage implements OnInit {

  private user: User | null = null;
  //  public user: any;
  private desabilitaRaca = true;
  private desabilitaCidade = true;
  public estados: Array<any>;
  public cidades: Array<any>;
  private comparacao: (a: number, b: number) => boolean;

  constructor(
    private usuarioService: UsuarioService,
    private authService: AuthService,
    private alertService: AlertService,
    private animaisService: AnimaisService,
    private sanitizer: DomSanitizer,
    private navCtrl: NavController,
    private sessaoService: SessaoService
  ) {
    console.info('init');
    // this.sessaoService.usuario.subscribe((usuario: User) => {
    //   this.user = usuario;
    // })
    // console.info('estalogado');
    // console.info(authService.estaLogado());
    console.info('sessaoService.getUsuario()');
    console.info(sessaoService.getUsuario());
    // console.info('sessaoService.getUsuario()');
    // console.info(sessaoService.getUsuario());
    // this.usuarioService.getUsuario(7).subscribe((data: any) => {
    //   console.info('data');
    //   console.info(data);
    //   let retorno = (data as any)._body;
    //   data["foto"] = this.sanitizer.bypassSecurityTrustUrl(data["foto"]);

    //   this.user = data;
    //   console.info('this.usuario');
    //   console.info(this.user);

    // }, error => {
    //   console.log(error);
    // })
  }

  ngOnInit() {
    this.animaisService.getEstados().subscribe((data: any) => {
      let retorno = (data as any)._body;
      this.estados = data;

    }, error => {
      console.log(error);
    })
  }

  editar(formValue: NgForm) {
    console.info('editar entrou');
    console.info(formValue);
    this.mostraMensagem();
    this.usuarioService.editaUsuario(formValue.value)
      .subscribe(data => {
      }, error => {
        console.log(error);
      });
  }

  async mostraMensagem() {
    this.alertService.presentToast("Usuário editado!");
    this.navCtrl.navigateRoot('/meus-animais');
  }

  ionViewDidEnter() {
    // this.authService.user().subscribe(
    //   user => {
    //     this.usuario = user;
    //   }
    // );
    this.sessaoService.usuario.subscribe((usuario: User) => {
      this.user = usuario;
    })
    console.info('user');
    console.info(this.user);
    this.comparacao = (a, b) => {
      return a === b;
    };
  }

  busca_cidades(id_estado) {
    this.animaisService.getCidades(id_estado).subscribe((data: any) => {
      let retorno = (data as any)._body;
      this.cidades = data;
      this.desabilitaCidade = false;
    }, error => {
      console.log(error);
    })
  }
}
