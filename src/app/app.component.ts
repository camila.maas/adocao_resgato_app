import { Component } from '@angular/core';
import { Platform, NavController, Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './services/auth.service';
import { AlertService } from './services/alert.service';
import { User } from './models/user';
import { SessaoService } from './services/sessao.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public loginPages = [
    {
      title: 'Entrar',
      url: 'login',
      icon: 'log-in'
    },
    {
      title: 'Cadastrar-se',
      url: 'register',
      icon: 'add'
    }
  ];

  public loggedPages = [
    {
      title: 'Perfil',
      url: 'dados',
      icon: 'log-in'
    },
    {
      title: 'Meus animais',
      url: 'meus-animais',
      icon: 'add'
    }
  ];

  public appPages = [
    {
      title: 'Encontrar animais',
      url: 'busca',
      icon: 'search'
    },
    {
      title: 'Eventos',
      url: 'lista-eventos',
      icon: 'calendar'
    },
    {
      title: 'Ong',
      url: 'lista-ong',
      icon: 'people'
    },
    {
      title: 'Sobre',
      url: 'sobre',
      icon: 'information'
    },
  ];

 private user: User |null=null;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService,
    public events: Events,
    public sessaoService: SessaoService
  ) {
    this.initializeApp();
    events.subscribe('user:created', (user) => {
      console.info('bem vindo' + this.user.email);
    })
  }

  initializeApp() {
    console.info();
    this.platform.ready().then(() => {
      this.sessaoService.getUsuario().then((usuario)=>{
        this.sessaoService.usuario.next(usuario);
      });

      this.sessaoService.usuario.subscribe((usuario:User)=>{
        this.user = usuario;
      })
      this.statusBar.styleDefault();
      // Commenting splashScreen Hide, so it won't hide splashScreen before auth check
      //this.splashScreen.hide();
      this.authService.getToken();
    });
    console.info('this.user');
    console.info(this.user);
  }

  // When Logout Button is pressed 
  logout() {
    this.authService.logout();
    this.navCtrl.navigateRoot('/busca');
  }

  ionViewDidEnter() {
    console.info('user menu');
    // console.info(this.user);
    // this.authService.user().subscribe(
    //   user => {
    //     this.user = user;
    //   }
    // );
  }

}