import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnvService } from '../env.service';

@Injectable({
  providedIn: 'root'
})
export class OngService {

  constructor(public http: HttpClient,
    private env: EnvService) { }

  getOng(){
    return this.http.get(this.env.API_URL +"ong/");
  }

  getDetalhesOng(id){
    return this.http.get(this.env.API_URL +"ong/find/"+id);
  }
}
