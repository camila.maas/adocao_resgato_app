import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvService } from './env.service';
import { User } from '../models/user';
import { SessaoService } from './sessao.service';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  isLoggedIn = false;
  token: any;
  private user: User | null = null;

  constructor(
    private http: HttpClient,
    private storage: NativeStorage,
    private env: EnvService,
    public sessaoService: SessaoService
  ) { }
  async login(email: String, password: String) {
    return await new Promise((res, rej) => {
      this.http.post(this.env.API_URL + 'auth/login',
        { email: email, password: password }
      ).subscribe(
        (data: any) => {
          this.storage.setItem('token', data.data.token)
            .then(
              () => {
                console.log('Token Stored');
              },
              error => console.error('Error storing item', error)
            );
          this.token = data.data.token;
          this.isLoggedIn = true;
          res(data.data);
        }, err => rej(err));
    });
  }

  register(fName: String, lName: String, email: String, password: String) {
    return this.http.post(this.env.API_URL + 'auth/register',
      { fName: fName, lName: lName, email: email, password: password }
    )
  }
  logout() {
    this.sessaoService.setUsuario(null);

    // const headers = new HttpHeaders({
    //   'Authorization': this.token["token_type"]+" "+this.token["access_token"]
    // });
    // return this.http.get(this.env.API_URL + 'auth/logout', { headers: headers })
    // .pipe(
    //   tap(data => {
    //     this.storage.remove("token");
    //     this.isLoggedIn = false;
    //     delete this.token;
    //     return data;
    //   })
    // )
  }
  // user() {
  //   const headers = new HttpHeaders({
  //     'Authorization': this.token["token_type"]+" "+this.token["access_token"]
  //   });
  //   return this.http.get<User>(this.env.API_URL + 'auth/me', { headers: headers })
  //   .pipe(
  //     tap(user => {
  //       console.info('user auth.service');
  //       console.info(user);
  //       return user;
  //     })
  //   )
  // }
  getToken() {
    return this.storage.getItem('token').then(
      data => {
        this.token = data;
        if (this.token != null) {
          this.isLoggedIn = true;
        } else {
          this.isLoggedIn = false;
        }
      },
      error => {
        this.token = null;
        this.isLoggedIn = false;
      }
    );
  }

  async estaLogado() {
    return (await this.sessaoService.getUsuario() ? true : false);
  }
}