import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnvService } from '../env.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(public http: HttpClient,
    private env: EnvService) { }

  getUsuario(id) {
    return this.http.get(this.env.API_URL +"usuarios/find/" + id);
  }

  editaUsuario(postData) {
    return this.http.post(this.env.API_URL +"usuarios/save/", postData);
  }
  
}
