import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnvService } from '../env.service';

@Injectable({
  providedIn: 'root'
})
export class EventosService {

  constructor(public http: HttpClient,
    private env: EnvService) { }

  getEventos(){
    return this.http.get(this.env.API_URL +"eventos/");
  }

  getDetalhesEvento(id){
    return this.http.get(this.env.API_URL +"eventos/find/"+id);
  }
}
