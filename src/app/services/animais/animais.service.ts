import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { provideRoutes } from '@angular/router';
import { ProtractorExpectedConditions } from 'protractor';
import { EnvService } from '../env.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class AnimaisService {

  public animais = [];
  public loading;
  public refresher;
  public isRefreshing: boolean = false;

  constructor(public http: HttpClient,
    public loadingController: LoadingController,
    private sanitizer: DomSanitizer,
    private env: EnvService) {
    // console.log('entrou service');
  }

  getAnimais() {
    return this.http.get(this.env.API_URL + "animais/");
  }

  getAnimaisTipo(tipo) {
    return this.http.get(this.env.API_URL + "animais/", { params: { tipo: tipo } });
  }

  // buscarFiltro(tipo, raca, porte, sexo) {
  //   return this.http.get(this.env.API_URL +"animais/search/", { params: { tipo: tipo, raca: raca, porte: porte, sexo: sexo } });
  // }

  buscarFiltro(filter) {
    return this.http.post(this.env.API_URL + "animais/search/", filter);
  }

  getDetalhesAnimal(id) {
    return this.http.get(this.env.API_URL + "animais/find/" + id);
  }

  getRacasTipo(type) {
    return this.http.get(this.env.API_URL + "racas/find_by_type/" + type);
  }

  getEstados() {
    return this.http.get(this.env.API_URL + "estados/");
  }

  getCidades(id_estado) {
    return this.http.get(this.env.API_URL + "cidades/find_by_state_id/" + id_estado);
  }

  salvaAnimal(postData) {
    return this.http.post(this.env.API_URL + "animais/save/", postData);
  }

  getAnimaisUsuario(user_id) {
    return this.http.get(this.env.API_URL + "animais/get_all_by_user/" + user_id);
  }

  public carregarAnimais() {
    // console.info('entrou serice');
    this.presentLoading();

    this.getAnimais().subscribe(
      (data: any) => {
        this.animais = data;

        this.animais.forEach(a => a.foto = this.sanitizer.bypassSecurityTrustUrl(a.foto));
        // console.info('this.animais service');
        // console.info(this.animais);
        // console.info(this.isRefreshing);

        if (this.isRefreshing) {
          this.refresher.target.complete();
          this.isRefreshing = false;
        }
      }, error => {
        // console.log(error);
        if (this.isRefreshing) {
          this.refresher.target.complete();
          this.isRefreshing = false;
        }
      });
  }

  public carregarAnimaisTipo(tipo) {
    // this.presentLoading();
    this.animais = [];
    let animais = this.getAnimaisTipo(tipo).subscribe(
      (data: any) => {
        console.info('this.animais service antes');
        console.info(this.animais);
        this.animais = data;
        console.info('this.animais service');
        console.info(this.animais);
        this.animais.forEach(a => a.foto = this.sanitizer.bypassSecurityTrustUrl(a.foto));

        if (this.isRefreshing) {
          this.refresher.target.complete();
          this.isRefreshing = false;
        }
        window.localStorage.setItem('animais', JSON.stringify(this.animais));

      }, (error: any) => {
        // console.log(error);
        if (this.isRefreshing) {
          this.refresher.target.complete();
          this.isRefreshing = false;
        }
      });
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Carregando animais',
      duration: 2000
    });
    console.log('show service');
    await this.loading.present();
    const { role, data } = await this.loading.onDidDismiss();

    // console.log('Loading dismissed!');
  }

  //tentativa crud
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

}

