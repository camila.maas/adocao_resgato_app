import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class SessaoService {

  public usuario: BehaviorSubject<User | null> = new BehaviorSubject(null);
  public storageKey;

  constructor(
    private nativeStorage: NativeStorage,
  ) {
  }

  async setUsuario(usuario: User) {
    //DESCOMENTAR
    await this.usuario.next(usuario);
    
    if (usuario) {
      this.storageKey = `${usuario.id}`;
      return this.nativeStorage.setItem(this.storageKey, JSON.stringify(usuario))
        .then(() => {
          this.nativeStorage.setItem('usuario', JSON.stringify(usuario));
        })
        .catch((e) => console.log(e));
    } else {
      return this.nativeStorage.setItem('usuario', JSON.stringify(usuario))
        .catch((e) => console.log(e));
    }
  }

  async getUsuario() {
    return this.nativeStorage.getItem('usuario')
      .then((usuario) => {
        return JSON.parse(usuario);
      });
  }

  
}
