import { Component, ViewChild, OnInit } from '@angular/core';
import { CalendarComponent } from 'ionic2-calendar/calendar';
import { EventosService } from 'src/app/services/eventos/eventos.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.page.html',
  styleUrls: ['./lista.page.scss'],
})
export class ListaPage implements OnInit {

  event = {
    title: '',
    desc: '',
    startTime: '',
    endTime: '',
    allDay: false
  }
  minDate = new Date().toISOString();

  eventSource = [];
  calendar = {
    mode: 'month',
    currentDate: new Date()
  }
  month = '';

  // monthNames = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

  viewTitle = '';

  @ViewChild(CalendarComponent, null) myCal: CalendarComponent;

  constructor(private eventosService: EventosService,
    private navCtrl: NavController
  ) {

  }

  ngOnInit() {
    this.resetEvent();
    this.eventosService.getEventos().subscribe(
      (data: any) => {
        data.forEach(evento => {
          this.eventSource.push({
            id: evento.id,
            title: evento.nome,
            startTime: new Date(evento.data_hora_inicio),
            endTime: new Date(evento.data_hora_fim),
            allDay: false
          });

        });

      }, error => {
        console.log(error);
      });
  }

  resetEvent() {
    this.event = {
      title: '',
      desc: '',
      startTime: new Date().toISOString(),
      endTime: new Date().toISOString(),
      allDay: false
    }
  }

  addEvent() {
    let eventCopy = {
      title: this.event.title,
      desc: this.event.desc,
      startTime: new Date(this.event.startTime),
      endTime: new Date(this.event.endTime),
      allDay: this.event.allDay
    }

    if (eventCopy.allDay) {
      let start = eventCopy.startTime;
      let end = eventCopy.endTime;
      eventCopy.startTime = new Date(Date.UTC(start.getUTCFullYear(), start.getUTCMonth(), start.getUTCDate()));
      eventCopy.endTime = new Date(Date.UTC(start.getUTCFullYear(), start.getUTCMonth(), start.getUTCDate() + 1));
    }

    this.eventSource.push(eventCopy);
    this.myCal.loadEvents();
    this.resetEvent();
  }


  changeMode(mode) {
    this.calendar.mode = mode;
  }

  back() {
    var swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slidePrev();
  }

  next() {
    var swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slideNext();
  }

  today() {
    this.calendar.currentDate = new Date();
  }

  async onEventSelected(event) {
    console.info('detalhes');
    console.info(event);
    this.navCtrl.navigateForward('/detalhes-evento/' + event.id);

  }

  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  onTimeSelected() {
  }

}
