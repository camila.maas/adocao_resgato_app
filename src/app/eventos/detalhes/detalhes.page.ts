import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { EventosService } from 'src/app/services/eventos/eventos.service';

@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.page.html',
  styleUrls: ['./detalhes.page.scss'],
})
export class DetalhesPage implements OnInit {
public evento;

  constructor(public route: ActivatedRoute,
    private sanitizer: DomSanitizer,
    public eventosService: EventosService) {
    this.route.paramMap.subscribe((params: ParamMap) => {
      console.log("id recebido: " + params.get("id"));
      this.eventosService.getDetalhesEvento(params.get("id")).subscribe(data => {

        let retorno = (data as any)._body;
        data["get_fotos"].forEach(a => a.caminho = this.sanitizer.bypassSecurityTrustUrl(a.caminho));
        console.info('data');
        console.info(data);
        this.evento = data;

      }, error => {
        console.log(error);
      })
    })
  }

  ngOnInit() {
  }

}
