import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { OngService } from 'src/app/services/ong/ong.service';

@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.page.html',
  styleUrls: ['./detalhes.page.scss'],
})
export class DetalhesPage implements OnInit {
public ong;

  constructor(public route: ActivatedRoute,
    private sanitizer: DomSanitizer,
    public ongService: OngService) {
    this.route.paramMap.subscribe((params: ParamMap) => {
      console.log("id recebido: " + params.get("id"));
      this.ongService.getDetalhesOng(params.get("id")).subscribe(data => {
        
        let retorno = (data as any)._body;
        data["get_fotos"].forEach(a=>a.caminho = this.sanitizer.bypassSecurityTrustUrl(a.caminho));
        data["logo"] = this.sanitizer.bypassSecurityTrustUrl(data["logo"]);
        console.info('data');
        console.info(data);
        this.ong = data;

      }, error => { 
        console.log(error);
      })
    })
   }

  ngOnInit() {
    console.info('entrou detalhes ong');
  }

}
