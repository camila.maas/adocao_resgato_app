import { Component, OnInit } from '@angular/core';
import { OngService } from '../../services/ong/ong.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.page.html',
  styleUrls: ['./lista.page.scss'],
})
export class ListaOngPage implements OnInit {

  public ong: Array<any>;

  constructor(public ongService: OngService,
    private sanitizer: DomSanitizer,
    ) { }

  ngOnInit() {
    this.carregarOng();
  }

  carregarOng() {
    console.info('carregarOng');
    this.ongService.getOng().subscribe(
      (data: any) => {
        this.ong = data;
        console.log('this.ong');
        console.log(this.ong);
        this.ong.forEach(a=>a.logo = this.sanitizer.bypassSecurityTrustUrl(a.logo));

      }, error => {
        console.log(error);
      });
  }

}
