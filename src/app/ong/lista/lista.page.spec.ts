import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaOngPage } from './lista.page';

describe('ListaOngPage', () => {
  let component: ListaOngPage;
  let fixture: ComponentFixture<ListaOngPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaOngPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaOngPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
