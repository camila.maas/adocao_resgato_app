import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, Events } from '@ionic/angular';
import { RegisterPage } from '../register/register.page';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';
import { SessaoService } from 'src/app/services/sessao.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    private modalController: ModalController,
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService,
    private sessaoService: SessaoService,
    public events: Events
  ) { }

  ngOnInit() {
  }

  // Dismiss Login Modal
  dismissLogin() {
    this.modalController.dismiss();
  }

  // On Register button tap, dismiss login modal and open register modal
  async registerModal() {
    this.dismissLogin();
    const registerModal = await this.modalController.create({
      component: RegisterPage
    });
    return await registerModal.present();
  }

  login(form: NgForm) {
    this.authService.login(form.value.email, form.value.password).then(
      (data: any) => {

        let usuario: User = new User;
        usuario.id = data.id;
        usuario.nome = data.nome;
        usuario.email = data.email;
        this.alertService.presentToast("Bem vindo(a)");
        console.info('data');
        console.info(data);

        this.sessaoService.setUsuario(usuario);
      }).catch(
        error => {
          console.log(error);
        },
      ).finally(() => {
        this.dismissLogin();
        this.navCtrl.navigateRoot('/busca');
      }
      );
  }
}